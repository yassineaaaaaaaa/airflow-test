from datetime import datetime
from airflow import DAG
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from kubernetes.client import models as k8s




dag = DAG('hello_world2', description='Hello World DAG',
          schedule_interval='0 12 * * *',
          start_date=datetime(2017, 3, 20), catchup=False)


kub_operator =   KubernetesPodOperator(
    namespace="qse-ass01",
    image='registry-docker.apps.eul.sncf.fr/04567.dev/comob:0.0.0.6',
    image_pull_secrets=[k8s.V1LocalObjectReference('regcred-qse-ass01-secret-dev')],
    service_account_name='sa-qse-ass01',
    name='comob-job',
    arguments=[
        '/opt/spark/bin/spark-submit',
        '--master=k8s://kubernetes.default.svc',
        '--deploy-mode=cluster',
        '--name=comob',
        '--class=com.sncf.tn.qse.ssg.cdcbibigdata.comob_v2.business.Ingestion.enquete_od.EnqueteODIngestionJob',
        '--conf=spark.executor.instances=4',
        '--conf=spark.executor.memory=10G',
        '--conf=spark.kubernetes.container.image.pullSecrets=regcred-qse-ass01-secret-dev',
        '--conf=spark.executor.cores=2',
        '--conf=spark.kubernetes.namespace=qse-ass01',
        '--conf=spark.kubernetes.container.image.pullPolicy=Always',
        '--conf=spark.kubernetes.container.image=registry-docker.apps.eul.sncf.fr/04567.dev/comob:0.0.0.6',
        '--conf=spark.kubernetes.authenticate.driver.serviceAccountName=sa-qse-ass01',
        '--conf=spark.jars.ivy=/tmp/.ivy',
        '--conf=spark.kubernetes.file.upload.path=/tmp/',
        '--conf=spark.hadoop.fs.azure.account.key.sae2npfcsbt01.blob.core.windows.net=MA/YM2zLvDtped8lDGjTcEJBbD6xzdZUVxQtYAVRTmxRSea9TLg03jLJIy4yLOjWYcZyPzlJ9I+z7GG3t7sV1A==',
        '--conf=spark.hadoop.fs.azure=org.apache.hadoop.fs.azure.NativeAzureFileSystem',
        '--conf=spark.hadoop.fs.wasbs.impl=org.apache.hadoop.fs.azure.NativeAzureFileSystem',
        '--conf=spark.hadoop.fs.azure.delete.threads=5',
        '--conf=spark.hadoop.fs.azure.rename.threads=5',
        '/opt/spark/jars/comob-v2-backend-0.1.0.0-jar-with-dependencies.jar',
        '--jobName=comob-ingestion-enquete-od-job'
    ],
    in_cluster=True,
    task_id="task-two",
    get_logs=True, 
    is_delete_operator_pod=True,
    dag=dag
    )



kub_operator